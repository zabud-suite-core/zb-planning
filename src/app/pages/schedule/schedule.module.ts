import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ZbButtonsModule, ZbLayoutModule} from '@zb/suite-core';
import {ManageScheduleRoutingModule} from './manage-schedule-routing.module';
import { TableToggleDirective } from './directives/table-toggle.directive';
import { TableToggleItemDirective } from './directives/table-toggle-item.directive';
import {TableItemSelectComponent} from './components/table-item-select/table-item-select.component';
import {ManageScheduleContainerComponent} from './manage-schedule-container.component';
import {SubjectsByGroupComponent} from './components/subjects-by-group/subjects-by-group.component';
import {SectionContainerModule} from '../../components/section-container/section-container.module';

@NgModule({
  declarations: [
    TableToggleDirective,
    TableToggleItemDirective,
    ManageScheduleContainerComponent,
    TableItemSelectComponent,
    SubjectsByGroupComponent,
  ],
  imports: [
    ManageScheduleRoutingModule,
    CommonModule,
    ZbButtonsModule,
    ZbLayoutModule,
    SectionContainerModule
  ]
})
export class ScheduleModule { }
