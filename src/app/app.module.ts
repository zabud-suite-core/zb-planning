import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ZbButtonsModule, ZbDialogModule, ZbLayoutModule} from '@zb/suite-core';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, ZbDialogModule, ZbLayoutModule, ZbButtonsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
