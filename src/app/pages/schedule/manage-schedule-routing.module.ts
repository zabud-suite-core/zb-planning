import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageScheduleContainerComponent } from './manage-schedule-container.component';

const routes: Routes = [
  { path: '', component: ManageScheduleContainerComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageScheduleRoutingModule {}
