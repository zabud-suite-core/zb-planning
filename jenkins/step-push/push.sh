#!/bin/bash

echo "###########################"
echo "#### Preparing to push ####"
echo "###########################"

REGISTRY="192.168.3.2:5000"
IMAGE="zb-tronos-planning"

#echo "***** Logging in    ****"
#docker login -u cbarrera -p $PASS $REGISTRY
echo "***** Tagging image *****"
docker tag $IMAGE:$BUILD_TAG $REGISTRY/$IMAGE:$BUILD_TAG
echo "***** Pushing image *****"
docker push $REGISTRY/$IMAGE:$BUILD_TAG
