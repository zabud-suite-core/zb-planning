import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { IGroup } from '../../models/IGroup';

@Component({
  selector: 'group-list',
  templateUrl: './group-list.component.html',
  styles: [
    `
      .group:not(:last-child) {
        margin-bottom: 32px;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupListComponent {
  @Input() groups: IGroup[];
  @Input() teachers: any[];

  @Output() searchSubjects: EventEmitter<number> = new EventEmitter();
  @Output() updateSubjects: EventEmitter<any> = new EventEmitter();
}
