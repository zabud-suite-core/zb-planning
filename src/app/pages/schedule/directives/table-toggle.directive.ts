import {Directive, Output, EventEmitter} from '@angular/core';
import {TableToggleItemDirective} from './table-toggle-item.directive';
import {ISubject} from '../../groups/models/ISubject';

@Directive({
  selector: '[appTableToggle]'
})
export class TableToggleDirective {

  @Output() currentSubject = new EventEmitter<ISubject>();

  protected tableItems: Array<TableToggleItemDirective> = [];

  constructor() { }

  public closeOtherItems(openItem: TableToggleItemDirective): void {
    this.tableItems.forEach((item: TableToggleItemDirective) => {
      if (item !== openItem) {
        item.selected = false;
      }
    });
  }

  public selectedChange(item: TableToggleItemDirective): void {
    this.currentSubject.emit(item.subject);
  }

  public addItem(item: TableToggleItemDirective): void {
    this.tableItems.push(item);
  }
}
