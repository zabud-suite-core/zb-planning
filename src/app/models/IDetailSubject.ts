import {ISubject} from '../pages/groups/models/ISubject';

export interface IDetailSubject {
  groupSubject: ISubject;
  virtualClassroom: any;
}
