import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ISubject} from '../../../groups/models/ISubject';
import {ManageScheduleService} from '../../manage-schedule.service';
import {IDetailSubject} from '../../../../models/IDetailSubject';

@Component({
  selector: 'app-table-item-select',
  templateUrl: './table-item-select.component.html',
  styleUrls: ['./table-item-select.component.scss']
})
export class TableItemSelectComponent implements OnInit {

  @Output() currentDetailSubject = new EventEmitter<IDetailSubject>();

  @Input() subjects: ISubject[] = [];

  constructor(private services: ManageScheduleService) {
  }

  ngOnInit(): void {
  }

  onCurrentSubject(item: ISubject): void {
    this.services.findByIdSubjectDetail(item.id).subscribe(resp => {
      this.currentDetailSubject.emit(resp);
    });
  }
}
