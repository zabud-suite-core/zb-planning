FROM node:12.18.1-alpine as devbuild
WORKDIR /app
COPY . .
RUN ls
RUN npm install @angular/cli -g
RUN npm install
RUN npm run build:dev
FROM nginx
COPY --from=devbuild /app/dist/zb-planning/ /usr/share/nginx/html
COPY nginx-docker.conf /etc/nginx/nginx-docker.conf
EXPOSE 80