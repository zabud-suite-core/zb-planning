#!/bin/bash


echo "############################"
echo "****** Building image ******"
echo "############################"

docker-compose -f docker-compose-build.yml build --no-cache
