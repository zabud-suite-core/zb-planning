import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SubjectsResolver} from './pages/schedule/manage-schedule-resolvers.service';

const routes: Routes = [
  {
    path: 'groups',
    loadChildren: () =>
      import('./pages/groups/group.module').then(
        (m) => m.GroupManagementModule
      ),
  },
  {
    path: 'manage-schedule/:id',
    loadChildren: () => import('./pages/schedule/schedule.module').then((m) => m.ScheduleModule),
    resolve: {subjects: SubjectsResolver}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
