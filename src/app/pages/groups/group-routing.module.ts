import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupContainerComponent } from './group-container.component';

const routes: Routes = [
  { path: '', component: GroupContainerComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GroupRoutingModule {}
