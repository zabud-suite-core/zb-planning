import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageScheduleContainerComponent } from './manage-schedule-container.component';

describe('ManageScheduleContainerComponent', () => {
  let component: ManageScheduleContainerComponent;
  let fixture: ComponentFixture<ManageScheduleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageScheduleContainerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageScheduleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
