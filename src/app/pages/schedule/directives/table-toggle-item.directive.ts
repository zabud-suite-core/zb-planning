import {Directive, HostBinding, HostListener, Inject, Input} from '@angular/core';
import {TableToggleDirective} from './table-toggle.directive';
import {ISubject} from '../../groups/models/ISubject';

@Directive({
  selector: '[appTableToggleItem]'
})
export class TableToggleItemDirective {

  protected _selected: boolean;
  protected table: TableToggleDirective;

  @Input()
  subject: ISubject;

  @HostBinding('class.selected')
  @Input()
  get selected(): boolean {
    return this._selected;
  }

  set selected(value: boolean) {
    this._selected = value;
    if (value) {
      this.table.closeOtherItems(this);
      this.table.selectedChange(this);
    }
  }

  @HostListener('click', ['$event'])
  onClick(e: any): void {
    this.selected = !this.selected;
  }

  public constructor(@Inject(TableToggleDirective) table: TableToggleDirective) {
    this.table = table;
  }

  public ngOnInit(): any {
    this.table.addItem(this);
  }
}
