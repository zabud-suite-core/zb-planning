import {Component, OnInit, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-section-container',
  template:
    `
      <div class="section-container">
        <div class="title">
          <ng-content select="[title]"></ng-content>
        </div>
        <div class="box">
          <ng-content select="[box]"></ng-content>
        </div>
      </div>
  `,
  styles: [
    `
      .section-container {
        border-radius: 10px;
        overflow: hidden;
      }

      .section-container .title {
        display: flex;
        background-color: #2C82C7;
      }

      .section-container .title > span {
        width: 100%;
        padding-bottom: 10px;
        padding-top: 10px;
        text-align: center;
        color: white;
      }

      .section-container .box {
        min-height: 2rem;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.None
})

export class SectionContainerComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
