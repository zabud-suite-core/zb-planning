import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ISubject} from '../groups/models/ISubject';
import {HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {IDetailSubject} from '../../models/IDetailSubject';

@Injectable({
  providedIn: 'root'
})
export class ManageScheduleService {

  private baseUrl = `${environment.baseUrl}planning/groups`;
  constructor(private http: HttpClient) { }

  getSubjects(groupId: number): Observable<ISubject[]> {
    return this.http.get<ISubject[]>(`${this.baseUrl}/${groupId}/subjects`);
  }

  findByIdSubjectDetail(subjectId: number): Observable<IDetailSubject> {
    return this.http.get<IDetailSubject>(`${this.baseUrl}/subjects/${subjectId}`);
  }
}
