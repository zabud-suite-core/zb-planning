#!/bin/bash

echo "****************************"
echo "********* Testing **********"
echo "****************************"

docker run --rm -v /root/.m2:/root/.m2 -v /data/jenkins_home/workspace/Academico/Academico_develop/zb-tronos-planning:/zb-tronos-planning -w /zb-tronos-planning maven:3-alpine "$@"
