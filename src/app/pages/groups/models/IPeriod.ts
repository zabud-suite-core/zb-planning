export interface IPeriod {
  code: string;
  endDate: string;
  id: number;
  name: string;
  period: number;
  periodicity: string;
  starDate: string;
  status: string;
  year: number;
}
