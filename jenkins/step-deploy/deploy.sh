#!/bin/bash

## Generate file ##

echo zb-tronos-planning > /tmp/.auth
echo $BUILD_TAG >> /tmp/.auth
echo $PASS >> /tmp/.auth

## Transfer file ##

scp -i /opt/onpremises /tmp/.auth cbarrera@192.168.3.2:/tmp/.auth
scp -i /opt/onpremises ./jenkins/step-deploy/publish.sh cbarrera@192.168.3.2:/tmp/publish-zb-tronos-planning
ssh -i /opt/onpremises cbarrera@192.168.3.2 /tmp/publish-zb-tronos-planning
