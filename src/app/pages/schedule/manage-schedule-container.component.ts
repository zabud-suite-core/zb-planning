import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ISubject} from '../groups/models/ISubject';
import {IDetailSubject} from '../../models/IDetailSubject';

@Component({
  selector: 'app-manage-schedule-container',
  templateUrl: './manage-schedule-container.component.html',
  styleUrls: ['./manage-schedule-container.component.scss']
})
export class ManageScheduleContainerComponent implements OnInit {

  subjects: ISubject[] = [];
  currentDetailSubject: IDetailSubject;

  constructor(
    private routeGet: ActivatedRoute,
    private routeGo: Router,
  ) {
    this.subjects = this.routeGet.snapshot.data.subjects;
  }

  ngOnInit(): void {}

  goBackGroups () {
    this.routeGo.navigate([`groups`]);
  }

  oncurrentDetailSubject(detail: IDetailSubject): void {
    this.currentDetailSubject = detail;
  }

}
