import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IDetailSubject} from '../../../../models/IDetailSubject';
import {ISubject} from '../../../groups/models/ISubject';

@Component({
  selector: 'app-subjects-by-group',
  templateUrl: './subjects-by-group.component.html',
  styleUrls: ['./subjects-by-group.component.scss']
})
export class SubjectsByGroupComponent implements OnInit {

  @Output() currentDetailSubject = new EventEmitter<IDetailSubject>();

  @Input() subjects: ISubject[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  CurrentDetailSubject(item: IDetailSubject): void {
    this.currentDetailSubject.emit(item);
  }
}
