import { NgModule } from '@angular/core';
import {
  ZbAutocompleteModule,
  ZbCheckboxModule,
  ZbFormModule,
  ZbButtonsModule,
  ZbDialogModule,
  ZbLayoutModule,
  ZbPaginationModule,
  ZbTagsModule,
} from '@zb/suite-core';
import { CardModule } from 'src/app/components/card/card.module';
import { GroupComponent } from './components/group/group.component';
import { GroupRoutingModule } from './group-routing.module';
import { GroupListComponent } from './components/group-list/group-list.component';
import { GroupContainerComponent } from './group-container.component';
import { CommonModule } from '@angular/common';
import {SectionContainerModule} from '../../components/section-container/section-container.module';
import { CreateGroupDialogComponent } from './create-group/create-group-dialog.component';
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [GroupComponent, GroupListComponent, GroupContainerComponent, CreateGroupDialogComponent],
  imports: [
    CommonModule,
    CardModule,
    GroupRoutingModule,
    ZbButtonsModule,
    ZbDialogModule,
    ZbTagsModule,
    ZbLayoutModule,
    ZbPaginationModule,
    ZbFormModule,
    ZbAutocompleteModule,
    ZbCheckboxModule,
    ReactiveFormsModule,
    SectionContainerModule,
  ],
})
export class GroupManagementModule {}
