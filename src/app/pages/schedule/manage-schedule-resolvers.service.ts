import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {ManageScheduleService} from './manage-schedule.service';
import {ISubject} from '../groups/models/ISubject';


@Injectable({providedIn: 'root'})
export class SubjectsResolver implements Resolve<ISubject[]> {
  constructor(private services: ManageScheduleService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ISubject[]> | Promise<ISubject[]> | ISubject[] {
    const id: number = Number(route.params.id);
    return this.services.getSubjects(id);
  }
}
