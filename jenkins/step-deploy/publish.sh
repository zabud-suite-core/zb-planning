#!/bin/bash

export REGISTRY="192.168.3.2:5000"
export IMAGE=$(sed -n '1p' /tmp/.auth)
export TAG=$(sed -n '2p' /tmp/.auth)
#export PASS=$(sed -n '3p'/tmp/.auth)

#docker login -u cbarrera -p $PASS $REGISTRY

cd ~/jenkins/zb-tronos-planning/ && docker-compose -f docker-compose-zb-tronos-planning.yml up -d
